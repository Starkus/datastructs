#pragma once

#include "pch.h"

template <class T>
class vectorIter;

template <class T>
class vector {
	const int _default_cap = 8;
		
	T* a;
	int capacity;
	int size;

	void _moveArray(int newCap) {
		capacity = newCap;
		T* new_ar = new T[capacity];
		
		for (int i = 0; i < size; ++i) {
			new_ar[i] = a[i];
		}

		delete[] a;
		a = new_ar;
	}
	
public:
	typedef vectorIter<T> iterator;

	vector() {
		a = new T[_default_cap];
		capacity = _default_cap;
	}

	vector(int cap) {
		a = new T[cap];
		capacity = cap;
	}

	vector(vector& other) {
		for (int i = 0; i < other.size; ++i) {
			// Double the previous capacity
			a = new T[other.size() * 2];
			a[i] = other[i];
		}
	}

	void reserve(int l) {
		if (l > capacity) {
			_moveArray(l);
		}
	}

	void shrink_to_fit() {
		if (size != capacity) {
			_moveArray(size);
		}
	}

	void push_back(T item) {
		if (size + 1 >= capacity)
			_moveArray(capacity * 2);

		a[size] = item;
		size++;
	}

	void pop_back() {
		if (size <= 0) {
			throw std::length_error("Vector is empty");
		}

		size--;
	}

	iterator begin() {
		return iterator(a);
	}

	iterator end() {
		return iterator(a + size);
	}

	T& at(int index) {
		if (index < 0 || index >= size) {
			throw std::out_of_range("Index out of range");
		}

		return a[index];
	}

	T& operator[](int index) {
		return at(index);
	}
};

template <class T>
class vectorIter {
	T* p;

public:
	vectorIter(T* pointer) : p(pointer) {}

	T* operator&() const {
		return p;
	}

	T& operator*() const {
		return *p;
	}

	vectorIter<T> operator+(const vectorIter<T>& other) const {
		return vectorIter<T>(p + &other);
	}

	vectorIter<T> operator-(const vectorIter<T>& other) const {
		return vectorIter<T>(p - &other);
	}

	inline vectorIter<T> operator++() {
		return p++;
	}
	inline vectorIter<T> operator++(int o) {
		return p++;
	}

	inline vectorIter<T> operator--() {
		return p--;
	}
	inline vectorIter<T> operator--(int o) {
		return p--;
	}

	T* operator->() const {
		return p;
	}

	bool operator<(const vectorIter<T>& other) const {
		return p < &other;
	}
	bool operator>(const vectorIter<T>& other) const {
		return p > &other;
	}
	bool operator<=(const vectorIter<T>& other) const {
		return p <= &other;
	}
	bool operator>=(const vectorIter<T>& other) const {
		return p >= &other;
	}

	bool operator==(const vectorIter<T>& other) const {
		return p == &other;
	}
	bool operator!=(const vectorIter<T>& other) const {
		return p != &other;
	}
};

