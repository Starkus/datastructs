// CppConPipo.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <algorithm>
#include <memory>
#include <functional>
#include <string>
#include <assert.h>
#include <chrono>
#include <unordered_map>
#include <forward_list>

#include "hashtable.h"
#include "vector.h"
#include "linkedList.h"

using namespace std::chrono;

const int sz = 4096;

template <typename C>
void testContainer(std::string name, void (*fill)(C&)) {
	C container;
	auto t0 = high_resolution_clock::now();

	fill(container);
	auto t1 = high_resolution_clock::now();

	for (typename C::iterator i = container.begin(); i != container.end(); ++i) {}
	auto t2 = high_resolution_clock::now();

	std::cout << name << " took " << duration_cast<nanoseconds>(t1 - t0).count() << " nanoseconds to fill\n";
	std::cout << "took " << duration_cast<nanoseconds>(t2 - t1).count() << " nanoseconds to iterate through\n\n";
}

int main()
{
	testContainer<hashtable<std::string, int>>("Hashtable", [](hashtable<std::string, int>& c) {
	for (int i = 0; i < sz; ++i) {
		std::string str = std::to_string(i);
		c[str] = i;
	}
	});

	testContainer<std::unordered_map<std::string, int>>("std::unordered_map", [](std::unordered_map<std::string, int>& c) {
		for (int i = 0; i < sz; ++i) {
			std::string str = std::to_string(i);
			c[str] = i;
		}
	});

	testContainer<vector<int>>("Vector", [](vector<int>& c) {
		for (int i = 0; i < sz; ++i) {
			c.push_back(i);
		}
	});

	testContainer<std::vector<int>>("std::vector", [](std::vector<int>& c) {
		for (int i = 0; i < sz; ++i) {
			c.push_back(i);
		}
	});

	testContainer<linkedList<int>>("LinkedList", [](linkedList<int>& c) {
		for (int i = 0; i < sz; ++i) {
			c.push_back(i);
		}
	});

	testContainer<std::forward_list<int>>("std::forward_list", [](std::forward_list<int>& c) {
		for (int i = 0; i < sz; ++i) {
			c.push_front(i);
		}
	});

	return 0;
}
