#pragma once

#include <memory>

template <class T>
class listIter;

template <class T>
struct __listNode {
	__listNode(T value, std::shared_ptr<__listNode<T>> next) {
		this->value = value;
		this->next = next;
	}

	__listNode(const __listNode<T> &other) {
		this->value = other.value;
		if (other.next != nullptr) {
			this->next = std::make_unique<__listNode<T>>(*(other.next));
		}
	}

	__listNode(__listNode<T>&& other) {
		value = other.value;
		next = other.next;

		other.next = nullptr;
	}

	void del() {
		if (next != nullptr)
			next->del();
		next = nullptr;
	}

	~__listNode() {
		del();
	}

	T value;
	std::shared_ptr<__listNode<T>> next;
};

template <class T>
class linkedList {
protected:
	std::shared_ptr<__listNode<T>> root = nullptr;
	std::weak_ptr<__listNode<T>> last;

	std::weak_ptr<__listNode<T>> _getNode(int index) {
		std::weak_ptr<__listNode<T>> current = root;
		for (int i = 0; i < index; ++i) {
			current = current.lock()->next;
		}
		return current;
	}

	__listNode<T> *_getLastNode() {
		if (root == nullptr)
			return nullptr;
		__listNode<T>* current = root;
		while (current->next != nullptr) {
			current = current->next;
		}
		return current;
	}

public:
	typedef listIter<T> iterator;

	// Default constructor
	linkedList() {
		root = nullptr;
	}

	// Copy constructor
	linkedList(const linkedList<T>& other) {
		if (other.root != nullptr) {
			root = std::make_shared<__listNode<T>>(*(other.root));
			last = _getLastNode();
		}
	}

	// Move constructor
	linkedList(linkedList<T>&& other) {
		root = other.root;
		last = other.last;
		other.root = nullptr;
		other.last = nullptr;
	}

	~linkedList() {
//		if (root != nullptr)
//			root->del();

		// Prevent stack overflow when deleting a huge list
		// by avoiding recursion.
		std::vector<std::shared_ptr<__listNode<T>>> nodes;
		std::shared_ptr<__listNode<T>> current = root;
		while (current != nullptr) {
			nodes.push_back(current);
			current = current->next;
		}
		while (!nodes.empty()) {
			nodes.back()->next = nullptr;
			nodes.pop_back();
		}
	}

	void push_back(T v) {
		std::shared_ptr<__listNode<T>> new_node = std::make_unique<__listNode<T>>(v, nullptr);
		if (root == nullptr)
			root = new_node;
		else
			last.lock()->next = new_node;

		last = new_node;
	}

	void pop_back() {
		std::weak_ptr<__listNode<T>> secondToLast = _getNode(size() - 2);
		secondToLast->next = nullptr;
		last = secondToLast;
	}

	T at(int index) {
		return _getNode(index)->value;
	}

	int size() {
		int s = 0;
		__listNode<T> *current = root;
		while (current != nullptr) {
			s++;
			current = current->next;
		}
		return s;
	}

	iterator begin() {
		return iterator{ root };
	}

	iterator end() {
		return iterator{ last.lock() };
	}

	T operator[](int index) {
		return _getNode(index).lock()->value;
	}
};

template <class T>
class listIter {
	std::shared_ptr<__listNode<T>> p;

public:
	listIter(std::shared_ptr<__listNode<T>> pointer) : p(pointer) {}

	T& operator*() const {
		return p->value;
	}

	inline listIter<T> operator++() {
		p = p->next;
		return *this;
	}
	inline listIter<T> operator++(int o) {
		p = p->next;
		return *this;
	}

	inline listIter<T> operator--() = delete;

	bool operator==(const listIter<T>& other) const {
		return p == other.p;
	}
	bool operator!=(const listIter<T>& other) const {
		return p != other.p;
	}
};

