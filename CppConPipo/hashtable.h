#pragma once

#include <algorithm>
#include <memory>
#include <functional>
#include <string>
#include <assert.h>

template <class K, class V>
class hashIter;

template <class K, class V>
struct __hashnode {
	std::shared_ptr<__hashnode> next = nullptr;
	K key;
	V value;
};

template <class K, class V>
class hashtable {
	using node = __hashnode<K, V>;

	std::shared_ptr<node> *buckets;
	std::hash<K> hasher;

	std::shared_ptr<node> __getNodeByKey(K key, bool create) {
		unsigned int hash = hasher(key);
		hash = hash % bucketCount;

		std::shared_ptr<node> current = buckets[hash];

		if (current == nullptr) {
			buckets[hash] = std::make_shared<node>();
			current = buckets[hash];
			current->key = key;
		}

		while (true) {
			if (current->key == key) {
				return current;
			}
			if (current->next == nullptr) {
				if (create) {
					current->next = std::make_shared<node>();
					current->next->key = key;
					return current->next;
				}
				else {
					//throw std::runtime_error("Key don't exist!");
					return nullptr;
				}
			}
			current = current->next;
		}
		assert(0); // Code unreachable
	}

public:
	// TODO Non hard-coded bucket count
	const int bucketCount = 32;

	typedef hashIter<K, V> iterator;
	hashtable() {
		buckets = new std::shared_ptr<node>[bucketCount];
	}

	hashtable(const hashtable& other) {
		buckets = new node[bucketCount];
		std::copy(std::begin(other.buckets), std::end(other.buckets), std::begin(buckets));
	}

	hashtable(hashtable&& other) {
		buckets = other.buckets;
		other.buckets = nullptr;
	}

	~hashtable() {
		delete[] buckets;
	}

	void put(K key, V value) {
		std::shared_ptr<node> n = __getNodeByKey(key, true);

		n->value = value;
	}

	V get(K key) {
		std::shared_ptr<node> n = __getNodeByKey(key, false);
		if (n == nullptr) {
			throw std::invalid_argument("Key doesn't exist");
		}
		else {
			return n->value;
		}
	}

	int hashKey(K key) {
		return hasher(key) % bucketCount;
	}

	iterator begin() {
		return iterator{ *this, buckets, buckets[0] };
	}

	iterator end() {
		return iterator{ *this, buckets, nullptr };
	}

	V& operator[](K key) {
		return (*__getNodeByKey(key, true)).value;
	}
};

template <class K, class V>
class hashIter {
	hashtable<K, V> &table;
	std::shared_ptr<__hashnode<K, V>>* buckets;
	std::shared_ptr<__hashnode<K, V>> node;

public:
	hashIter(hashtable<K, V> &table, std::shared_ptr<__hashnode<K, V>>* array, std::shared_ptr<__hashnode<K, V>> node) : table(table), buckets(array), node(node) {}

	std::pair<K, V> operator*() const {
		return std::pair<K, V> {node->key, node->value};
	}

	hashIter<K, V> operator++() {
		hashIter<K, V> old = *this;

		// Try to advance within the bucket
		if (node->next != nullptr) {
			node = node->next;
			return old;
		}
		// If it's over and there's a next bucket, jump to it's first element
		else {
			int thisKey = table.hashKey(node->key);
			for (int b = thisKey + 1; b < table.bucketCount; ++b) {
				// Search for the next non-empty bucket
				node = buckets[b];
				if (node != nullptr)
					return old;
			}
			node = nullptr;
			return old;
		}
	}
	inline hashIter<K, V> operator++(int o) {
		return ++(*this);
	}

	hashIter<K, V> operator--() = delete; // These are forward iterators
	hashIter<K, V> operator--(int) = delete;

	bool operator<(const hashIter<K, V>& other) const {
		if (table.hashKey(node->key) < table.hashKey(other.node->key))
			return true;
		return false;
	}
	bool operator>(const hashIter<K, V>& other) const {
		if (table.hashKey(node->key) > table.hashKey(other.node->key))
			return true;
		return false;
	}
	bool operator<=(const hashIter<K, V>& other) const {
		return !((&this) > other);
	}
	bool operator>=(const hashIter<K, V>& other) const {
		return !((&this) < other);
	}

	bool operator==(const hashIter<K, V>& other) const {
		return node == other.node || (node == nullptr && other.node == nullptr);
	}
	bool operator!=(const hashIter<K, V>& other) const {
		return node != other.node && !(node == nullptr && other.node == nullptr);
	}
};

